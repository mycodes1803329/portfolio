jQuery(document).ready(function() {
  jQuery(".card.content-container > div.card-body:not('#aboutMe')").hide();

  showContent('aboutMe');

  jQuery("div.list-group a").click(function(e) {
    e.preventDefault();
    let target = jQuery(this).data("target");
    showContent(target);
  });

  function showContent(contentID) {
    jQuery(".card.content-container > div.card-body").hide();
    jQuery("#" + contentID).show();
  }
});



